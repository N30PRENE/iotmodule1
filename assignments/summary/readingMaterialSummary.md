## Industrial Revolution: IoT came to light

 - **Industry 1.0 (1784):** Mechanisation, Steam power & Weaving loom.
 
 - **Industry 2.0 (1870):** Mass Production, Assembly line & Electrical energy.
 
 - **Industry 3.0 (1969):** Automation, Computers & Electronics.
 
	 - Data is stored in databases and represented in excel sheets.
	 
	 - ### **Architecture:**
	 
		 - Field devices
			
			 - Sensors
			 - Actuators
			 - Remote Terminal Unit (RTU)
		 
		 - Control devices
		 
			 - Computer Numerical Control (CNC)
			 - Programmable Logic Controller (PLC)
			 - Distributed Control System (DCS)
			 
		 - Stations
		 
		 - Work centers (SCADA, HISTORIAN)
		 
		 - Enterprise
		
			 - Enterprise Resource Planning (ERP)
			 - Manufacturing Execution System (MES)
		
 		 - The bridge between the field devices and the control devices is called Field Bus.
		 
		 - **Communication protocols:** 
		 
		 Field Buses use several communication protocols like Modbus, CanOpen, EtherCAT, etc., are optimized for sending data to a central server inside a factory.
		 
		 ![Industry 3.0 Architecture](extras/assignments_summary_assets_industry-3.png)
		 
 - **Industry 4.0 (Today):** Cyber Physical Systems, IoT & Network.
 
	 - In a very basic sense, Industry 4.0 is Industry 3.0 devices connected to the Internet, which is what we, very elegantly call `IoT`.
	 
	 - The main purpose of connected devices to the internet is the ability to send and recieve data.
	 
	 - This therefore brings in various core functionality to these devices:
	 
		 - Dashboards: It transforms, displays & organizes a collection of data captured & transmitted by devices connected to the internet.

		 - Remote Web SCADA: Enables users to remotely monitor & control remote devices.

		 - Remote control configuration of devices.

		 - Predictive maintenance.

		 - Real-time event processing.

		 - Analytics with predictive models.

		 - Automated device provisioning (Auto discovery).

		 - Real-time alerts & alarms.
		 
	 - ### **Architecture:**
	 
		 ![Industry 4.0 Architecture](extras/assignments_summary_assets_industry-4.png)
		 
		**Communication protocols:** 
		
		`MQTT.org`, `AMQP`, `OPC UA`, `CoAP RFC 7252`, `HTTP`, `WebSockets`, `REST*ful* API`, etc., are optimized for sending data to cloud for data analysis.
		 
## Issues faced by Industry 4.0:
		
 - **Cost:** Industry 4.0 devices are expensive.
	 
 - **Downtime:** Changing hardware calls for big factory downtime.

 - **Unreliable devices:** Might pose circumstances to invest in devices which are unproven or unreliable.

### **However, there is a solution!**

Get data from Industry 3.0 devices/meters/sensors without changes to the original device. Further, we can use Industry 4.0 devices to send data to the cloud.

## The Solution: Getting data from Industry 3.0 device

 - ### Convert Industry 3.0 protocols into Industry 4.0 protocols.

	 - These conversions can be done using libraries available to get data from Industry 3.0 devices and further send it to Industry 4.0 devices.

	 ![Instances](extras/assignments_summary_assets_instances.png)

	 - ### Challenges faced during conversion:

	 	 - Expensive hardware.
	
		 - Lack of documentation.

		 - Proprietary PLC protocols.

	 - ### Procedure: 
	 	
		1. Identify most popular Industry 3.0 devices.

		2. Study protocols that these devices communicate.

		3. Get data from the Industry 3.0 device.

		4. Send the data to cloud for Industry 4.0 device.

## What next?

 Analyze data using various tools available online:

 - **IoT TSDB Tools**

 	These tools are used to store our data in the form of Time Series Databases. In a very basic sense, Time series data are simply measurements or events that are tracked, monitored, downsampled, and aggregated over time. This could be server metrics, application performance monitoring, network data, sensor data, events, clicks, trades in a market, and many other types of analytics data.

	*For example, Prometheus, InfluxDB, etc.*

 - **IoT Dashboards**

	Dashboards allow us to view our data in beautiful dashboards.

	*For example, Grafana, Thingsboard, etc.*

 - **IoT Platforms**

	Platforms allow us to analyse our data.

	*For example, AWS IoT, Google IoT, Azure IoT, Thingsboard, etc.*

 - **Recieve Alerts**

	Zaiper, Twilio, etc., allow us to get alerts based on our data using these platforms.
